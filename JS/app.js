function liveTabs() {

  let tabsList = document.querySelector('.tabs');
  let tabsContent = document.querySelector('.tabs-content');
    
    tabsList.addEventListener('click', function (event) {
      
        if (event.target.classList.contains('tabs-title')) {
      let index = Array.from(tabsList.children).indexOf(event.target);
      for (let i = 0; i < tabsList.children.length; i++) {
        tabsList.children[i].classList.remove('active');
        tabsContent.children[i].classList.remove('active');
            }
            
      event.target.classList.add('active');
            tabsContent.children[index].classList.add('active');
            
    }
  });
}


liveTabs();
